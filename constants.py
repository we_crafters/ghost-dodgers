#Consists of all the constants for the game

SCREEN_WIDTH = 570
SCREEN_HEIGHT = 570
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
RED = (255, 0, 0)
CELL_SIZE = 30
PACMAN_START_X = 1
PACMAN_START_Y = 1
scatter_targets = [(1, 1), (1, 17), (17, 1), (17, 17)] 
