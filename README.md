# Trouble Escapers

Welcome to the **Trouble Escapers** project! This is an immersive game that challenges your wit and reflexes. Choose from two exciting themes and embark on a journey filled with fun and surprises.

## Features

- **Interactive Gameplay**: Engage with two thrilling game themes:
  - **Theme 1: It's Halloween Time** - Experience the spooky excitement of Halloween with ghouls and ghosts. Can you escape the haunted maze?
  - **Theme 2: Let's Hack** - Step into the shoes of a digizen. Navigate through a digital maze and outsmart the hacking hats to win.

- **Responsive Design**: The application features a user-friendly interface that adapts to different screen sizes for an optimal experience on both desktop and mobile devices.

- **Flask Backend**: The server-side logic is handled by Flask, providing a smooth gameplay experience and easy integration of future features.

- **Asynchronous Game Start**: The game can be started with a button click that triggers a Python script in the background, making it interactive and dynamic.

## Installation

To run the Trouble Escapers game locally, follow these steps:

1. **Clone the repository:**
   ```bash
   git clone https://gitlab.com/we_crafters/ghost-dodgers.git
   cd ghost-dodgers

2. **Set up a virtual environment (optional but recommended):**
   ```bash
   python3 -m venv venv
   source venv/bin/activate  # On Windows use `venv\Scripts\activate`

3. **Install the required packages:**
   ```bash
   pip install -r requirements.txt

## Usage

1. **Start the Flask application:**
   ```bash
   python app.py

2. **Open your web browser and naviagte to:**
   ```arduino
   http://127.0.0.1:5000/

3. **Explore the game:**
   - Once the page loads, you will see the welcome message and options to start the game.
   - Click the button to begin your adventure!
